# Animal gallery

## Purpose

This project is an assigment in a web development course at NTNU. The task
was to create a website where the user can browse collections of images, text
and audio by choosing from a set of categories.
We were not allowed to use Typescript or any UI-libraries.

## Choices

### Technology used

- Javascript
- React
- Syntactically Awesome Stylesheet (Sass)
  - We chose to write styles with Sass (.scss) in order to improve readability and avoid having to repeat ourselves when defining CSS-classes. Sass is not considered a framework (it does not add any additional level of abstraction), but rather an extension language for making CSS simpler to write.

### Functionality

The following is a list of user-features which have been implemented in the project:

- [x] Play audio from a loaded audio-element
- [x] Choose a category for which image, audio and text to display
- [x] Interactable log (history) of previously chosen combinations of categories
- [x] Remembering the user's last chosen combination upon revisitting the page
- [x] For the chosen category for a specific media type, the user can browse through all entries of the chosen category.
- [x] Responsive design which lets the user access all functionality on the site -- even when using a narrow screen.

#### Browser storage

We chose to manually cache all fetched data in order to reduce network overhead. Audio could not be fetched easily, but this is cached by the browser by default:

![alt text](common/cached_audio.png "Audio being cached in browser")

Session storage is used for displaying the previous choices which has been made by the user while the tab has been open.

Local storage is used for remembering the user's last choice in order to display it at next visit.

#### Functional components vs class components

In order to be consistent, we chose to use <em>functional</em> components for things that are mainly being "displayed", and <em>class</em> components for more state- and logic-heavy components such as the Content-component. Syntax-wise, functional components look better and have less boilerplate code.

### Styling

#### Layout

We have placed the category picker and the history view in a sidepanel in order to give the main focus to the content itself. A sidepanel is also easy to adapt to a mobile screen. Since we were not allowed to fetch the content of all the compositions at once, we chose to use numbered pagination instead of tabs with the names of the compositions or thumbnails.
Here is an image showing the page's main layout:

![alt text](common/screenshot_1.PNG "Default layout")

#### Style sheets

We have used viewport height and width for the main layout in order to easily resize the page for different screens. Media rules are used to determine the layout on a mobile screen. When these are triggered, the art is arranged in a column to save space, and the side-panel is hidden behind a button.

|                     Narrow view                      |                  Narrow view (with panel extended)                  |
| :--------------------------------------------------: | :-----------------------------------------------------------------: |
| ![alt text](common/screenshot_2.PNG "Narrow layout") | ![alt text](common/screenshot_3.PNG "Narrow layout and side-panel") |

#### Structure

Structure-wise, we chose to have the styling definitions in their own files with the same name as the component they apply to. Global styles have lowercase names and use category-names in order to organize them properly.

### Component structure

The App-component is the top component and contains a header and the content itself.

The Content-component contains the art, a sidepanel for changing the categories and viewing the history of the last choices, and a paginator for navigating between the compositions.

The Sidepanel-component contains a Tabs-component for navigating between the category form and the history of the last choices. The forms for picking a category for each media type are represented by the CategoryPicker component in order to avoid repetition. History entries are represented by a component for the same reason.

### Testing

#### Component testing

As far as testing components go, we have mostly tested that the components render
without crashing, and we have used unit testing on the CategoryPicker component.
Some simple snapshot tests are also utilized to ensure that components do not
change in unexpected ways between versions. Snapshot tests are mostly considered
for the more simple components. Larger components (containing other smaller components)
were not as suitable for snapshot tests in this project, since we wanted as little coupling in the
codebase as possible.

If you would like to run these tests yourself, you can run
`npm test`
in the terminal (You should of course make sure that you have <em>npm</em> installed.) Make sure you are inside the proper directory "webutvikling-project-2":  
<br>
You can also run tests individually by, for example, running:
`jest src.Content.test.js`
(Note that you have to specify the test's path and use <em>jest</em> instead of <em>npm</em>)

#### Browser testing

We've verified that the browser works as expected in the following browsers:

- Firefox
- Chrome
- Vivaldi
- Edge
- Safari
- Kiwi Browser (phone)
- Chrome for Android
- Firefox mobile

This covers the most commonly used evergreen browser engines (Gecko, Webkit, and Blink), so few users should experience problems.

#### Device testing

The website has been tested on the following devices:

Laptops and desktops:

- Lenovo Yoga 520 (14")
- Lenovo Yoga 530 (14")
- Brandless desktop (25")

Phones:

- Oneplus 6T (6.41")
- Motorola G7 Plus (6.2")

### Usage of Gitlab and Git

We've made and assigned issues for all the main requirements and features of the project. In order to connect branches and commits to these issues, we have referenced them in both commits and merge requests.

We've created a branch for each feature and hot-fix in order to maintain a good workflow, and we've used merge requests for reviewing branches before merging to master.

## **\*\*\*\***End of student documentation**\*\*\*\***

---

---

---

# React auto-generated documentation

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
