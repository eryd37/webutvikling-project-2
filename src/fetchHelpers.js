export async function fetchOrUseCache(path) {
	try {
		// Try to get the resource from existing cache
		const cachedResponse = await caches.match(path);
		if (cachedResponse === undefined) {
			// Fetch from the path and put it in cache
			const cache = await caches.open(path);
			// Save cache with path as key
			await cache.add(path);
			// Get the newly saved cache by path and return
			return await caches.match(path);
		} else {
			return cachedResponse;
		}
	} catch (error) {
		// Fetch normally if browser refuses to use cache (private mode)
		return await fetch(path);
	}
}

export async function getSvgFromPath(path) {
	const response = await fetchOrUseCache("assets/svg/" + path);
	return await response.text();
}

export async function getJsonFromPath(path) {
	const response = await fetchOrUseCache("assets/" + path);
	return await response.json();
}
