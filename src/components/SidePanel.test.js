import React from "react";
import ReactDOM from "react-dom";
import { cleanup, render } from "@testing-library/react";
import SidePanel from "./SidePanel";

afterEach(cleanup);

it("renders without crashing", () => {
	const container = document.createElement("div");
	ReactDOM.render(
		<SidePanel
			categoryChoice={{
				image: "dogs",
				audio: "dogs",
				text: "dogs"
			}}
			history="[]"
		/>,
		container
	);
});
