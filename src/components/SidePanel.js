import React, { useState } from "react";
import "./SidePanel.scss";
import CategoryPicker from "./CategoryPicker";
import Tabs from "./Tabs";
import HistoryEntry from "./HistoryEntry";
import { PropTypes } from "prop-types";

const SidePanel = props => {
	const choices = props.categoryChoice;
	const [imageCategory, setImageCategory] = useState(choices.image);
	const [audioCategory, setAudioCategory] = useState(choices.audio);
	const [textCategory, setTextCategory] = useState(choices.text);
	const [activeTab, setActiveTab] = useState("category");

	const categories = {
		image: imageCategory,
		audio: audioCategory,
		text: textCategory
	};
	const now = new Date();
	const history = JSON.parse(props.history);
	if (history) history.sort((a, b) => (a.timestamp > b.timestamp ? -1 : 1));

	const isChanged = () => {
		return (
			choices.image !== imageCategory ||
			choices.audio !== audioCategory ||
			choices.text !== textCategory
		);
	};

	return (
		<div className={"side-panel" + (props.open ? " side-panel-expanded" : "")}>
			<div className="side-panel-content">
				<Tabs activeTab={activeTab} handleTabClick={setActiveTab} />

				<div className="side-panel-tabs">
					<div
						className={
							"category-tab" + (activeTab === "category" ? " active" : "")
						}
					>
						<h4 className="tab-title">Pick categories</h4>
						<CategoryPicker
							mediaType={"Image"}
							category={imageCategory}
							setCategory={setImageCategory}
						/>
						<CategoryPicker
							mediaType={"Audio"}
							category={audioCategory}
							setCategory={setAudioCategory}
						/>
						<CategoryPicker
							mediaType={"Text"}
							category={textCategory}
							setCategory={setTextCategory}
						/>
						<button
							className="button"
							onClick={() => props.handleUse(categories)}
							disabled={!isChanged()}
						>
							Use choices
						</button>
					</div>
					<div
						className={
							"history-tab" + (activeTab === "history" ? " active" : "")
						}
					>
						<h4 className="tab-title">History</h4>
						{history ? (
							<div>
								{history.map((entry, index) => {
									return (
										<HistoryEntry
											key={index}
											entryImage={entry.choices.image}
											entryAudio={entry.choices.audio}
											entryText={entry.choices.text}
											timestamp={entry.timestamp}
											now={now}
											setCategories={props.handleUse}
										/>
									);
								})}
							</div>
						) : (
							<div>
								<p>There is no history yet. Go make some!</p>
							</div>
						)}
					</div>
				</div>
			</div>
		</div>
	);
};
export default SidePanel;

PropTypes.SidePanel = {
	handleUse: PropTypes.func.isRequired,
	categoryChoice: PropTypes.object.isRequired,
	history: PropTypes.object.isRequired,
	open: PropTypes.bool.isRequired
};
