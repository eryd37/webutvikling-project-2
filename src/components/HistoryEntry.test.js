import React from "react";
import TestRenderer from "react-test-renderer";
import { cleanup, render } from "@testing-library/react";
import HistoryEntry from './HistoryEntry';

afterEach(cleanup);

it("renders without crashing", () => {
	render(<HistoryEntry entryImage="Dogs" entryAudio="Dogs" entryText="Dogs" timestamp={new Date()} now={new Date()} setCategories={() => {}} />);
});

// Snapshot test fro this component, 26 sep 2019 15:19
it("matches snapshot", () => {
	const tree = TestRenderer.create(
		<HistoryEntry entryImage="Dogs" entryAudio="Dogs" entryText="Dogs" timestamp={new Date()} now={new Date()} setCategories={() => {}} />
	).toJSON();
	expect(tree).toMatchSnapshot();
});