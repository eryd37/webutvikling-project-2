import React from "react";
import "./SidePanelButton.scss";
import { PropTypes } from "prop-types";

const SidePanelButton = props => {
	return (
		<div>
			<div
				className={
					"sidepanel-button-wrapper" +
					(props.open ? " sidepanel-button-wrapper-open" : "")
				}
			>
				<button
					className={"sidepanel-button"}
					onClick={() => props.handleClick(!props.open)}
				>
					<i className={"material-icons" + (props.open ? " icon-flipped" : "")}>
						double_arrow
					</i>
				</button>
			</div>
			{props.open ? (
				<div
					className="dark-sidepanel-background"
					onClick={() => props.handleClick(false)}
				></div>
			) : (
				undefined
			)}
		</div>
	);
};
export default SidePanelButton;

PropTypes.SidePanelButton = {
	open: PropTypes.bool.isRequired,
	handleClick: PropTypes.func.isRequired
};
