import React from "react";
import TestRenderer from "react-test-renderer";
import { cleanup, render } from "@testing-library/react";
import Tabs from "./Tabs";

afterEach(cleanup);

it("renders without crashing", () => {
	render(<Tabs activeTab="category" />);
});

it("renders if no props", () => {
	render(<Tabs />);
});

// Snapshot test fro this component, 26 sep 2019 15:19
it("matches snapshot with props", () => {
	const tree = TestRenderer.create(<Tabs activeTab="category" />).toJSON();
	expect(tree).toMatchSnapshot();
});

it("matches snapshot without props", () => {
	const tree = TestRenderer.create(<Tabs />).toJSON();
	expect(tree).toMatchSnapshot();
});
