import React from "react";
import PropTypes from "prop-types";
import "./Page.scss";

const Page = props => {
	return (
		<div
			className={
				"page-number" + (props.activePage === props.label ? " active" : "")
			}
			onClick={() => props.onClickPage(props.label)}
		>
			<span className="page-number-text">{props.label}</span>
		</div>
	);
};

export default Page;

Page.propTypes = {
	activePage: PropTypes.string.isRequired,
	label: PropTypes.string.isRequired,
	onClickPage: PropTypes.func.isRequired
};
