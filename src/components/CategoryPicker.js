import React from "react";
import "./CategoryPicker.scss";
import { PropTypes } from "prop-types";

const CategoryPicker = props => {
	return (
		<div className="radio-group">
			<div className="radio-group-title">
				<h5>{props.mediaType}</h5>
			</div>
			<div className="radio-input">
				<label>
					<input
						type="radio"
						checked={props.category === "birds"}
						onChange={() => {
							props.setCategory("birds");
						}}
					/>
					Birds
				</label>
			</div>
			<div className="radio-input">
				<label>
					<input
						type="radio"
						checked={props.category === "cats"}
						onChange={() => {
							props.setCategory("cats");
						}}
					/>
					Cats
				</label>
			</div>
			<div className="radio-input">
				<label>
					<input
						type="radio"
						checked={props.category === "dogs"}
						onChange={() => {
							props.setCategory("dogs");
						}}
					/>
					Dogs
				</label>
			</div>
		</div>
	);
};
export default CategoryPicker;

PropTypes.CategoryPicker = {
	setCategory: PropTypes.func.isRequired,
	category: PropTypes.string.isRequired,
	mediaType: PropTypes.string.isRequired
};
