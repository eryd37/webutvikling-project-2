import React from "react";
import "./HistoryEntry.scss";
import { PropTypes } from "prop-types";

const HistoryEntry = props => {
	const localDate = new Date(props.timestamp);
	const secondsAgo = Math.round((props.now - localDate) / 1000);
	const minutesAgo = Math.round(secondsAgo / 60);
	const hoursAgo = Math.round(minutesAgo / 60);
	const daysAgo = Math.round(hoursAgo / 24);

	let timestamp = "";

	if (hoursAgo >= 24) {
		if (daysAgo === 1) {
			timestamp = "yesterday";
		} else {
			timestamp = daysAgo + " days ago";
		}
	} else if (minutesAgo >= 60) {
		if (hoursAgo === 1) {
			timestamp = hoursAgo + " hour ago";
		} else {
			timestamp = hoursAgo + " hours ago";
		}
	} else if (secondsAgo >= 60) {
		if (minutesAgo === 1) {
			timestamp = minutesAgo + " minute ago";
		} else {
			timestamp = minutesAgo + " minutes ago";
		}
	} else {
		timestamp = "just now";
	}

	return (
		<div
			className="entry"
			onClick={() => {
				props.setCategories({
					image: props.entryImage,
					audio: props.entryAudio,
					text: props.entryText
				});
			}}
		>
			<div className="choice-wrapper">
				<i className="material-icons">image</i>
				<p>{props.entryImage}</p>
			</div>
			<div className="choice-wrapper">
				<i className="material-icons">volume_up</i>
				<p>{props.entryAudio}</p>
			</div>
			<div className="choice-wrapper">
				<i className="material-icons">short_text</i>
				<p>{props.entryText}</p>
			</div>
			<div className="timestamp-wrapper">
				<p>{timestamp}</p>
			</div>
		</div>
	);
};
export default HistoryEntry;

PropTypes.HistoryEntry = {
	entryImage: PropTypes.string.isRequired,
	entryAudio: PropTypes.string.isRequired,
	entryText: PropTypes.string.isRequired,
	timestamp: PropTypes.instanceOf(Date).isRequired,
	now: PropTypes.instanceOf(Date).isRequired,
	setCategories: PropTypes.func.isRequired
};
