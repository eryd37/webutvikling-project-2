import React from "react";
import PropTypes from "prop-types";

import "./Paginator.scss";
import Page from "./Page";

const Paginator = props => {
	// Fancy way of making ["1", "2", "3", "4"]
	const pageList = Array.from(Array(4), (e, i) => "" + (i + 1));
	return (
		<div className="paginator-wrapper">
			<div className="paginator">
				<div className="page-list">
					{pageList.map((label, index) => {
						return (
							<Page
								activePage={props.activePage}
								key={index}
								label={label}
								onClickPage={props.onClickPage}
							/>
						);
					})}
				</div>
			</div>
		</div>
	);
};

export default Paginator;

Page.propTypes = {
	activePage: PropTypes.string.isRequired,
	onClickPage: PropTypes.func.isRequired
};
