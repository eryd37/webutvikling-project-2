import React from "react";
import ReactDOM, { unmountComponentAtNode } from "react-dom";
import CategoryPicker from "./CategoryPicker";
import { render, getByText, getByLabelText } from "@testing-library/react";
import { act } from "react-dom/test-utils";

let container = null;
beforeEach(() => {
	container = document.createElement("div");
	document.body.appendChild(container);
});

afterEach(() => {
	unmountComponentAtNode(container);
	container.remove();
	container = null;
});

it("renders without crashing", () => {
	ReactDOM.render(<CategoryPicker />, container);
});

it("renders with the given props", () => {
	act(() => {
		ReactDOM.render(
			<CategoryPicker
				mediaType={"Test"}
				category={"dogs"}
				setCategory={text => {}}
			/>,
			container
		);
	});
	expect(getByText(container, "Test")).not.toBeUndefined();
	expect(getByLabelText(container, "Dogs").checked).toBeTruthy();
	expect(getByLabelText(container, "Cats").checked).toBeFalsy();
	expect(getByLabelText(container, "Birds").checked).toBeFalsy();
});
