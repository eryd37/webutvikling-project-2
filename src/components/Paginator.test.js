import React from "react";
import TestRenderer from "react-test-renderer";
import { cleanup, render } from "@testing-library/react";
import Paginator from "./Paginator";

afterEach(cleanup);

it("renders without crashing", () => {
	render(<Paginator activePage="1" onClickPage={() => {}} />);
});

// Snapshot test fro this component, 26 sep 2019 15:19
it("matches snapshot", () => {
	const tree = TestRenderer.create(
		<Paginator activePage="1" onClickPage={() => {}} />
	).toJSON();
	expect(tree).toMatchSnapshot();
});
