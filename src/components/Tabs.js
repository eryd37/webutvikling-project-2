import React from "react";
import "./Tabs.scss";
import { PropTypes } from "prop-types";

const Tabs = props => {
	return (
		<div className="tabs-wrapper">
			<div>
				<div className="tabs">
					<div
						className={
							"tab" + (props.activeTab === "category" ? " tab-active" : "")
						}
						onClick={() => props.handleTabClick("category")}
					>
						Category
					</div>
					<div
						className={
							"tab" + (props.activeTab === "history" ? " tab-active" : "")
						}
						onClick={() => props.handleTabClick("history")}
					>
						History
					</div>
				</div>
				<div
					className={
						"tab-indicator" + (props.activeTab === "history" ? " right" : "")
					}
				></div>
			</div>
		</div>
	);
};
export default Tabs;

PropTypes.Tabs = {
	activeTab: PropTypes.string.isRequired,
	handleTabClick: PropTypes.func.isRequired
};
