import React, { Component } from "react";
import "./Content.scss";
import SidePanel from "./components/SidePanel";
import SidePanelButton from "./components/SidePanelButton";
import Paginator from "./components/Paginator";
import { getJsonFromPath, getSvgFromPath } from "./fetchHelpers";

class Content extends Component {
	constructor(props) {
		super(props);
		let startupChoices = {
			image: "birds",
			audio: "birds",
			text: "birds"
		};
		const lastChoices = this.getLastChoices();
		if (lastChoices) {
			startupChoices = lastChoices;
		}
		this.state = {
			svg: undefined,
			text: undefined,
			loaded: false,
			sidepanelOpen: false,
			categoryChoice: startupChoices,
			activePage: "1",
			history: null
		};
	}

	componentDidMount() {
		let categoryChoice = this.state.categoryChoice;
		const existingHistory = sessionStorage.getItem("choiceHistory");
		let newState = {};
		if (existingHistory) {
			newState = { history: JSON.parse(existingHistory) };
		}
		this.loadMedia(categoryChoice, this.state.activePage).then(response => {
			newState.svg = response.svg;
			newState.text = response.text;
			newState.creditsSVG = response.creditsSVG;
			newState.creditsAudio = response.creditsAudio;
			newState.loaded = true;
			newState.categoryChoice = response.categoryChoice;
			this.setState(newState);
		});
	}

	setLastChoices(categoryChoice) {
		localStorage.setItem("imageChoice", categoryChoice.image);
		localStorage.setItem("audioChoice", categoryChoice.audio);
		localStorage.setItem("textChoice", categoryChoice.text);
	}

	getLastChoices() {
		const imageChoice = localStorage.getItem("imageChoice");
		const audioChoice = localStorage.getItem("audioChoice");
		const textChoice = localStorage.getItem("textChoice");
		if (imageChoice && audioChoice && textChoice) {
			return { image: imageChoice, audio: audioChoice, text: textChoice };
		} else {
			return undefined;
		}
	}

	async loadMedia(categoryChoice, activePage) {
		const imageChoice = categoryChoice.image;
		const textChoice = categoryChoice.text;
		const audioChoice = categoryChoice.audio;
		let svg = undefined;
		let text = undefined;
		let jsonSVG = undefined;
		let jsonAudio = undefined;
		// Making a list of promises in order to run them at the same time
		let fetchPromises = [];
		fetchPromises.push(
			(svg = await getSvgFromPath(
				`${imageChoice}/${imageChoice}-${activePage}.svg`
			))
		);
		fetchPromises.push(
			(text = await getJsonFromPath(
				`text/${textChoice}/${textChoice}-${activePage}.json`
			))
		);
		fetchPromises.push(
			(jsonSVG = await getJsonFromPath(
				`svg/${imageChoice}/${imageChoice}-${activePage}.json`
			))
		);
		fetchPromises.push(
			(jsonAudio = await getJsonFromPath(
				`audio/${audioChoice}/${audioChoice}-${activePage}.json`
			))
		);
		// Waiting until all promises have been resolved before returning
		return Promise.all(fetchPromises).then(async () => {
			return {
				svg: svg,
				text: text,
				creditsSVG: jsonSVG.credits,
				creditsAudio: jsonAudio.credits,
				loaded: true,
				categoryChoice: categoryChoice,
				activePage: activePage
			};
		});
	}

	handleChangeCategory = categories => {
		if (categories.image && categories.audio && categories.text) {
			const now = new Date();
			const newHistoryEntry = { choices: categories, timestamp: now };
			this.setLastChoices(categories);
			// Making a default history with this first entry in case there is none in storage
			let history = [newHistoryEntry];
			if (sessionStorage.getItem("choiceHistory")) {
				// Using the existing history and adding to it
				// It will be a string which has to be converted to an object using JSON.parse()
				let parsedHistory = JSON.parse(sessionStorage.getItem("choiceHistory"));
				parsedHistory.push(newHistoryEntry);
				history = parsedHistory;
			}
			// Sessionstorage can only handle strings, so we need to stringify our history object
			sessionStorage.setItem("choiceHistory", JSON.stringify(history));
			const newHistory = sessionStorage.getItem("choiceHistory");
			this.loadMedia(categories, this.state.activePage).then(response => {
				this.setState({
					history: JSON.parse(newHistory),
					svg: response.svg,
					text: response.text,
					creditsSVG: response.creditsSVG,
					creditsAudio: response.creditsAudio,
					loaded: true,
					categoryChoice: response.categoryChoice
				});
			});
		}
	};

	handleChangePage = page => {
		this.loadMedia(this.state.categoryChoice, page).then(response => {
			this.setState({
				svg: response.svg,
				text: response.text,
				creditsSVG: response.creditsSVG,
				creditsAudio: response.creditsAudio,
				loaded: true,
				activePage: page
			});
		});
	};

	handleClickToggleSidePanel = open => {
		this.setState({ sidepanelOpen: open });
	};

	render() {
		const audioChoice = this.state.categoryChoice.audio;
		return (
			<div className="base-container">
				<SidePanel
					handleUse={this.handleChangeCategory}
					categoryChoice={this.state.categoryChoice}
					history={JSON.stringify(this.state.history)}
					open={this.state.sidepanelOpen}
				/>
				<SidePanelButton
					open={this.state.sidepanelOpen}
					handleClick={this.handleClickToggleSidePanel.bind(this)}
				/>
				<div className="main-container">
					<div className="content">
						{this.state.loaded ? (
							<div className="loaded-content">
								<div className="content-image-sound">
									<div className="content-horizontal-align">
										<div
											className="content-image"
											dangerouslySetInnerHTML={{ __html: this.state.svg }}
										></div>
										<div className="content-text-credits">
											Credits: {this.state.creditsSVG}
										</div>
										<div className="audio-wrapper">
											<div className="content-sound">
												<audio
													controls
													src={`${process.env.PUBLIC_URL}/assets/audio/${audioChoice}/${audioChoice}-${this.state.activePage}.mp3`}
												></audio>
												<div className="content-text-credits">
													Credits: {this.state.creditsAudio}
												</div>
											</div>
										</div>
									</div>
								</div>
								<div className="content-text">
									<div className="content-text-title">
										<h3>{this.state.text.title}</h3>
									</div>
									<div className="content-text-block">
										{this.state.text.text}
									</div>
									<div className="content-text-credits">
										Credits: {this.state.text.credits}
									</div>
								</div>
								<Paginator
									activePage={this.state.activePage}
									onClickPage={this.handleChangePage.bind(this)}
								/>
							</div>
						) : (
							<div></div>
						)}
					</div>
				</div>
			</div>
		);
	}
}
export default Content;
