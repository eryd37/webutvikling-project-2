import React from "react";
import ReactDOM from "react-dom";
import Content from "./Content";
import { cleanup } from "@testing-library/react";

afterEach(cleanup);

it("renders without crashing", () => {
	const container = document.createElement("div");
	ReactDOM.render(<Content />, container);
});
