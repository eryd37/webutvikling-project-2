import React from "react";
import "./App.scss";
import Content from "./Content";

const App = () => {
	return (
		<div className="App">
			<header className="header">
				<h1>Animal gallery</h1>
			</header>
			<Content />
		</div>
	);
};

export default App;
